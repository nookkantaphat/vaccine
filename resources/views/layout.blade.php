<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>BRH</title>
    <link rel="icon" href="{{asset('logo-icon.ico')}}">
    <link rel="stylesheet" href="{{asset('assets\bootstrap5\css\bootstrap.min.css')}}">
    <script src="{{asset('assets\bootstrap5\js\bootstrap.min.js')}}"></script>
    <!-- <link rel="stylesheet" href="    {{asset('assets\datepicker\jquery-ui.css')}}"> -->

    @yield('style')

    <style>


        .center-screen {
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            text-align: center;
            min-height: 100vh;
        }

        .font-vaccine {
            font-size: 20px;
            color: blue;
        }

        .grad {
            /* background: linear-gradient(to right, blue, lightblue); */
            background-color: #71D24F;
            color: white;
            font-size: 16px;
            font-weight: bold;
        }
    </style>
</head>

<body>
    <div class="container-fluid ">
        <div class=" fixed-top navbar navbar-light navbar-light bg-light">
            <div class="row ">
                <div class="col">
                    <a href="{{url('/')}}">
                        <img src="{{asset('assets\image\logo.png')}}" alt="" class="img-fluid">
                    </a>
                </div>
            </div>
        </div>
        @yield('body')
        <div class="fixed-bottom navbar navbar-light navbar-light bg-light">
            <div class="row">
                <div class="col">
                    <img src="{{asset('assets\image\logo_bottom.png')}}" alt="" class="img-fluid">
                </div>
            </div>
            <div class="col-12 mx-auto text-center" style=" border-top-left-radius: 20px; border-top-right-radius: 20px;">
                <small>
                    <b>COPYRIGHT © 2020.</b>
                </small>
            </div>
        </div>
    </div>
    <script src="{{asset('assets\jquery\jquery-3.6.0.min.js')}}"></script>
    <script src="{{asset('assets\swal\sweetalert2.js')}}"></script>
    <!-- <script src="{{asset('assets\datepicker\jquery-ui.js')}}"></script> -->

    @yield('script')
    <script>
        $(document).ready(function() {
            $('#VaccineRegister').on('click', function(e) {
                e.preventDefault();
                $('#verifyModal1').modal('toggle');
            });

            $(document).on('click', '.verify', function() {
                var modalid = $(this).parents('.modal').attr('id');
                if (modalid == 'verifyModal1') {
                    $('#verifyModal1').modal('toggle');
                    $('#verifyModal2').modal('toggle');
                } else if (modalid == 'verifyModal2') {
                    $('#verifyModal2').modal('toggle');
                    $('#verifyModal3').modal('toggle');
                } else {
                    location.replace("{{url('/register')}}");
                }
            });

            $(document).on('click', '.close', function() {
                var modalid = $(this).parents('.modal').attr('id');
                $('#' + modalid).modal('toggle');
            });


        });
    </script>
</body>

</html>