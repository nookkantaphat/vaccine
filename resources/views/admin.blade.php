<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Check Slip Vaccine Covid</title>
    <!-- Bootstrap CSS -->
    <link rel="icon" href="{{asset('logo-icon.ico')}}">
    <link rel="stylesheet" href="{{asset('assets\bootstrap5\css\bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/dataTable/jquery.dataTables.css')}}">


    <script src="{{asset('assets\bootstrap5\js\bootstrap.min.js')}}"></script>
    <style>
        table.dataTable thead .sorting {
            background-image: url("{{asset('assets/image/sort_both.png')}}");
        }

        table.dataTable thead .sorting_asc {
            background-image: url("{{asset('assets/image/sort_asc.png')}}") !important;
        }

        table.dataTable thead .sorting_desc {
            background-image: url("{{asset('assets/image/sort_desc.png')}}") !important;
        }
    </style>
</head>

<body>
    <div class="container-fluid">
        <div class=" fixed-top navbar navbar-light navbar-light bg-light">
            <div class="row " style="width: 100%;">
                <div class="col">
                    <div class="row col">
                        <a href="{{url('/')}}">
                            <img src="{{asset('assets\image\logo.png')}}" alt="" class="img-fluid">
                        </a>
                    </div>
                </div>
                <div class="col text-end align-middle">
                    <a href="{{url('/logout')}}" class="" id="logout">ออกจากระบบ</a>
                </div>
            </div>
        </div>
        <div class="row col-12 mx-auto " style="margin-top: 120px;">
            <table class="table my-2" id="table_id">
                <thead class="thead-dark">
                    <tr>
                        <th class="align-middle text-center">#</th>
                        <th class="align-middle text-center">วันเวลาที่โอน</th>
                        <th class="align-middle text-center">เข้าบัญชีธนาคาร</th>
                        <th class="align-middle text-center">จำนวนเงิน</th>
                        <th class="align-middle text-center">สถานะ</th>
                        <th class="align-middle text-center">หมายเหตุ</th>
                        <th class="align-middle text-center">รายละเอียด</th>
                    </tr>
                    <tr>
                        <th class="align-middle text-center"></th>
                        <th class="align-middle text-center"></th>
                        <th class="align-middle text-center"></th>
                        <th class="align-middle text-center"></th>
                        <th class="align-middle text-center">สถานะ</th>
                        <th class="align-middle text-center"></th>
                        <th class="align-middle text-center"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1;
                    foreach ($register as $row) : ?>

                        <tr>
                            <th class="align-middle text-center"><?= $i; ?></th>
                            <?php

                            $state = end($row->status);
                            $state_name = '';
                            if ($state->status_state == 1) {
                                $state_name = "กำลังจอง";
                            } else if ($state->status_state == 2) {
                                $state_name = "รอชำระเงิน";
                            } else if ($state->status_state == 3) {
                                $state_name = "ชำระเงินแล้ว";
                            } else if ($state->status_state == 5) {
                                $state_name = "ปฏิเสธการจอง";
                            } else if ($state->status_state == 4) {
                                $state_name = "ยืนยันการจอง";
                            } else if ($state->status_state == 6) {
                                $state_name = "หมดเวลาชำระเงิน";
                            }



                            // $time = 'ss';
                            foreach ($row->status as $rowp) {
                                $time = '';
                                if ($rowp->status_state == 3) {
                                    $time = $rowp->created_at;
                                    $time = strtotime($rowp->created_at);
                                    $time =  date('Y/m/d H:i:s', $time);
                                    break;
                                }
                            }

                            $remark = '';
                            if (isset($state->remark)) {
                                $remark = $state->remark;
                            }
                            $allprice = 0;
                            foreach ($row->reserve as $rowo) {
                                if ($rowo->active == 1) {
                                    $allprice += $rowo->dose * $rowo->price;
                                }
                            }

                            if (count($row->slip) != 0) {
                                $bank = 'ธนาคารไทยพาณิชย์<br>5233044679';
                            } else {
                                $bank = '';
                            }
                            ?>
                            <!-- 13/07/2021 19:47 น. -->
                            <td class="align-middle text-center"><?= $time ?></td>
                            <td class="align-middle text-center"><?= $bank ?></td>
                            <td class="align-middle text-center"><?= ($allprice != 0) ? number_format($allprice) . ' บาท' : ''; ?></td>
                            <td class="align-middle text-center"><?= $state_name ?> </td>
                            <td class="align-middle text-center"><?= $remark ?> </td>
                            <td class="align-middle text-center">
                                <button class="btn btn-outline-info btnModalDetail" id="btnModal<?= $i ?>" data-toggle="modal" data-target="#ModalDetail<?= $i ?>">เปิดดู</button>
                            </td>
                        </tr>
                    <?php $i++;
                    endforeach ?>
                
                </tbody>
            </table>
        </div>
    </div>
    <?php $j = 1;
    foreach ($register as $rowi) : ?>
        <?php $id = $rowi->_id; ?>
        <!-- Modal Detail -->
        <div class="modal fade" id="ModalDetail<?= $j ?>" tabindex="-1" role="dialog" aria-labelledby="ModalDetail<?= $j ?>Title" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered " role="document">
                <div class="modal-content">
                    <div class="modal-header bg-info">
                        <h5 class="modal-title fontTopic" id="ModalDetailTitle" style="color: white;">รายละเอียดการจอง</h5>
                        <button type="button" class="close btn btnCloseModal" id="btnCloseModal<?= $j ?>" style="margin: 0 0 5px 0;" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">X</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row  mx-auto">
                            <div class="col-12 col-sm-3 col-md-3 col-lg-2 col-xl-2  my-auto text-center">
                                <?php if (end($rowi->status)->status_state != 4 && end($rowi->status)->status_state != 5 && end($rowi->status)->status_state != 6) { ?>
                                    <button class="btn btn-outline-danger col col-sm-12 col-md-12 col-lg-12 col-xl-12 btnReject" id="btnReject<?= $j ?>" data-toggle="modal" data-target="#modalReject<?= $j ?>" data-dismiss="modal" style="cursor: pointer;">Reject</button>
                                <?php } ?>
                            </div>
                            <?php if (count($rowi->slip) != 0) {
                                $imgSlip = $urlImg . '' . $rowi->slip[0]->filename;
                            } else {
                                $imgSlip = '';
                            }
                            ?>
                            <div class="col-12 col-sm-6 col-md-6 col-lg-8 col-xl-8 mx-auto my-auto text-center">
                                <img src="<?= $imgSlip ?>" style="width: 60%;" class="img-fluid img-thumbnail" />
                            </div>
                            <div class="col-12 col-sm-3 col-md-3 col-lg-2 col-xl-2 my-auto text-center">
                                <?php if (end($rowi->status)->status_state != 4 && end($rowi->status)->status_state != 5 && end($rowi->status)->status_state != 6) { ?>
                                    <button class="btn btn-outline-primary col col-sm-12 col-md-12 col-lg-12 col-xl-12 btnConfirm" value="<?= $id ?>">Confirm</button>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-12 mx-auto table-responsive">
                            <table class="table" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th class="align-middle text-center">ลำดับ</th>
                                        <th class="align-middle text-center">ชื่อ-นามสกุล</th>
                                        <th class="align-middle text-center">จำนวน</th>
                                        <th class="align-middle text-center">ราคา</th>
                                        <th class="align-middle text-center">รวม</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $u = 1;
                                    foreach ($rowi->reserve as $rowu) : ?>
                                        <?php if ($rowu->active == 1) { ?>
                                            <tr>
                                                <td class="align-middle text-center"><?= $u ?></td>
                                                <td class="align-middle text-center">
                                                    <div class="col-12 mx-auto text-center"><?= $rowu->prename . ' ' . $rowu->firstname . ' ' . $rowu->lastname; ?></div>
                                                    <div class="col-lg-8 col-sm-12 mx-auto">
                                                        <img src="<?= $urlImg . '' . $rowu->cardimage ?>" style="width: 40%" />
                                                    </div>
                                                </td>
                                                <td class="align-middle text-center"><?= $rowu->dose ?></td>
                                                <td class="align-middle text-center"><?= number_format($rowu->price); ?></td>
                                                <td class="align-middle text-center"><?= number_format($rowu->dose * $rowu->price) ?></td>
                                            </tr>
                                    <?php $u++;
                                        }
                                    endforeach; ?>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- Modal Reject -->
        <div class="modal fade" id="modalReject<?= $j ?>" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-danger">
                        <h5>เหตุผลที่ reject ?</h5>
                        <button type="button" class="close btn btnCloseReject" id="btnCloseReject<?= $j ?>" style="margin: 0 0 5px 0;" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">X</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form id="formReject<?= $j ?>" class="formReject">
                            <input type="hidden" name="hd_registerId" class="hd_registerId" value="<?= $id ?>">
                            <textarea class="form-control" name="remark" placeholder="กรุณากรอกเหตุผลที่ Reject"></textarea>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <div class="col-12 mx-auto text-right">
                            <button class="btn btn-outline-danger submitReject" id="submitReject<?= $j ?>">บันทึก</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php $j++;
    endforeach; ?>

    <script src="{{asset('assets\jquery\jquery-3.6.0.min.js')}}"></script>
    <script src="{{asset('assets\swal\sweetalert2.js')}}"></script>
    <script type="text/javascript" charset="utf8" src="{{asset('assets/dataTable/jquery.dataTables.js')}}"></script>

    <script>
        $(document).ready(function() {

            var i = 0;
            $('#table_id').DataTable({
                orderCellsTop: true,
                initComplete: function() {
                    this.api().columns().every(function() {
                        if (i == 4) {
                            var column = this;
                            var select = $('<select class="form-control" style="border-color : black"><option value=""></option></select>')
                                .appendTo($("#table_id thead tr:eq(1) th").eq(column.index()).empty())
                                .on('change', function() {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                    );

                                    column
                                        .search(val ? '^' + val + '$' : '', true, false)
                                        .draw();
                                });

                            column.data().unique().sort().each(function(d, j) {
                                select.append('<option value="' + d + '">' + d + '</option>');
                            });
                        }
                        i++;
                    });
                }
            });

            $('#rejectBtn').on('click', function() {
                $('#formRegect').submit();
            });

            $(document).on('click', '.btnModalDetail', function() {
                var id = $(this).attr('id');
                id = id.substr(8);
                // alert(id);
                $('#ModalDetail' + id).modal('toggle');
            });



            $(document).on('click', '.btnReject', function() {
                var id = $(this).attr('id');
                id = id.substr(9)
                $('#modalReject' + id).modal('toggle');
            });

            $(document).on('click', '.btnCloseModal', function() {
                var id = $(this).attr('id');
                id = id.substr(13);
                $('#ModalDetail' + id).modal('toggle');
            });

            $(document).on('click', '.btnCloseReject', function() {
                var id = $(this).attr('id');
                id = id.substr(14)
                $('#modalReject' + id).modal('toggle');
            });

            $(document).on('click', '.submitReject', function() {
                var id = $(this).attr('id');
                id = id.substr(12);
                $('#formReject' + id).submit();
            });

            $(document).on('click', '.btnConfirm', function() {
                var regisId = $(this).val();
                $.ajax({
                    url: "{{url('confirm')}}",
                    headers: {
                        'X-CSRF-TOKEN': "{{csrf_token()}}"
                    },
                    method: "POST",
                    data: {
                        register_id: regisId
                    },
                    dataType: "JSON",
                    success: function(data) {
                        if (data.data.success == true) {
                            Swal.fire({
                                icon: 'success',
                                title: 'ยืนยันสำเร็จ',
                                text: 'ยืนยันการจองวัคซีน',
                                confirmButtonText: 'ปิด',
                                confirmButtonColor: '#A5DC86'
                            }).then((result) => {
                                location.reload();
                            });
                        }
                    }
                });
            });

            $(document).on('submit', '.formReject', function(e) {
                e.preventDefault();
                $.ajax({
                    url: "{{url('reject')}}",
                    headers: {
                        'X-CSRF-TOKEN': "{{csrf_token()}}"
                    },
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "JSON",
                    success: function(data) {
                        if (data.data.success == true) {
                            Swal.fire({
                                icon: 'error',
                                title: 'ยกเลิกสำเร็จ',
                                text: 'ยกเลิกการจองวัคซีน',
                                confirmButtonText: 'ปิด',
                                confirmButtonColor: '#F27474'
                            }).then((result) => {
                                location.reload();
                            });
                        }
                    }
                });
            });

        });
    </script>
</body>

</html>