@extends('layout')
@section('style')
<style>
    a li.list-group-item:hover {
        background-color: #dcdcde;
    }

    input[type=text] {
        border: none;
        border-bottom: 2px solid #808080;
    }

    input[type=number] {
        border: none;
        border-bottom: 2px solid #808080;
    }

    input:required {
        border: none;
        border-bottom: 2px solid red;
    }

    input:required:valid {
        border: none;
        border-bottom: 2px solid #067423;
    }

    #fdate_dob:valid+.labelDOB {
        visibility: visible !important;
    }

    .BKForm:before {
        opacity: 0.5;
    }

    .BorderInput {
        border: groove;
        border-color: deepskyblue;
        border-radius: 10px;
        background-color: white;
    }

    .FontTopic {
        font-size: x-large;
        text-shadow: 1px 1px 1px #0094ff;
    }

    .CheckboxSize {
        width: 1.5em;
        height: 1.5em;
    }

    .fontInvent {
        color: blue;
        font-size: 25px;
        font-family: 'Prompt', sans-serif;
        text-shadow: 2px 2px 2px #ffffff;
    }

    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    .textwrap {
        word-wrap: break-word;
        text-overflow: ellipsis;
    }

    .fontDetail {
        font-size: x-large;
    }
</style>
@stop

@section('body')
<div class="row col" style="margin-top: 60px; margin-bottom:100px;">
    <div class="col-12 mx-auto mt-5 alert alert-warning FontTopic">
        <?php $allPrice = 0;
        $allDose = 0;
        foreach ($reserve as $rowp) {
            $allPrice += ($rowp->dose * 1650);
            $allDose += $rowp->dose;
        } ?>
        จำนวนเงินที่ท่านต้องทำการโอนชำระทั้งหมด <?= number_format($allPrice); ?> บาท
        <span class="btn btn-outline-warning" id="btnModal" data-toggle="modal" data-target="#modalDetail" style="cursor: pointer; text-shadow: 1px 1px 1px #000000;">
            รายละเอียด
        </span>
    </div>
    <div class="col-12 mx-auto mb-3 alert alert-danger fontDetail text-center">
        กรุณาโอนเงินและแนบสลิปก่อนถึงเวลา {{$timeLast}} น.
    </div>
    <div class="row col-12 mx-auto ">
        <div class="col-11 mx-auto FontTopic text-center ">
            บัญชีธนาคารสำหรับการโอนชำระ
        </div>
        <div class="col col-lg-12 col-xl-6 mx-auto  text-center ">
            <div class="row">
                <div class="col-10 col-sm-6 col-md-5 col-lg-4 col-xl-5 mx-auto ">
                    <img src="{{asset('assets/image/scb.png')}}" class="imgCircle" width="70%" />
                </div>
                <div class="col mx-auto  fontDetail text-start">
                    ธนาคารไทยพาณิชย์ 5233044679 <br>โรงพยาบาลกรุงเทพระยอง
                </div>
            </div>
        </div>
    </div>
    <div class="row col-12 mx-auto my-3 FontTopic">
        แนบสลิปการโอน<span class="important">*</span>
        <div class="row col-12 mx-auto">
            <form id="formUploadImage">
                <div class="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4 " style="font-size: large;">
                    <input type="file" id="fileImg" name="fileImg" class="form-control" required />
                </div>
                <br>
                <div class="col-lg-3 col-sm-5 mx-auto">
                    <img src="{{asset('assets/image/pay1.png')}}" id="imgPreview" alt="slip" style="width: 100%" />
                </div>
            </form>
        </div>
        <form id="formPayment">
            <div class="row">
                <input type="hidden" name="hd_image" id="hd_image">
                <input type="hidden" name="hd_parentsId" id="hd_parentsId" value="<?php echo (isset($_GET['register_id'])) ? $_GET['register_id'] : ''; ?>">
                <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3  mx-auto my-3 FontTopic">
                    จำนวนเงินที่โอน<span class="important">*</span>
                    <input type="number" name="money" id="money" min="0" value="<?= $allPrice ?>" required class="form-control" />
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3  mx-auto my-3 FontTopic">
                    เวลาที่โอน<span class="important">*</span>
                    <input type="time" name="time" id="time" required class="form-control" />
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3 mx-auto my-3 FontTopic">
                    เบอร์โทรศัพท์<span class="important">*</span>
                    <input type="number" name="tel" id="tel" value="" required class="form-control" placeholder="เบอร์โทรศัพท์ ที่สามารถติดต่อได้" />
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3"></div>
            </div>
        </form>
    </div>
    <div class="col-12 mx-auto my-5 text-center ">
        <button type="button" id="btnFormPayment" class="btn btn-outline-primary FontTopic">ส่งสลิป</button>
    </div>


    <!-- Modal Detail-->
    <div class="modal fade" id="modalDetail" tabindex="-1" aria-labelledby="modalDetailLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <h5 class="modal-title fontTopic" id="modalDetailTitle">รายละเอียดการจอง</h5>
                    <button type="button" class="close btn" style="margin: 0 0 5px 0;" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">X</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-12 mx-auto">
                        <div class="row col-12 mx-auto table-responsive">
                            <table class="table">
                                <thead class="bg-dark " style="color: yellow;">
                                    <tr>
                                        <th>ลำดับ</th>
                                        <th>ชื่อ-นามสกุล</th>
                                        <th>จำนวน</th>
                                        <th>ราคา</th>
                                        <th>รวม</th>
                                    </tr>
                                </thead>
                                <?php $i = 1;
                                foreach ($reserve as $row) : ?>

                                    <tr>
                                        <td><?= $i ?></td>
                                        <td><?= $row->prename . ' ' . $row->firstname . ' ' . $row->lastname; ?></td>
                                        <td><?= $row->dose . ' เข็ม'; ?></td>
                                        <td>1,650</td>
                                        <?php $price = number_format(($row->dose * 1650)); ?>
                                        <td><?= $price ?></td>
                                    </tr>

                                <?php $i++;
                                endforeach; ?>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">

                </div>
            </div>
        </div>
    </div>


    <!-- Modal Submit -->
    <div class="modal fade" id="ModalSubmit" tabindex="-1" role="dialog" aria-labelledby="ModalSubmitTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <div class="modal-title fontTopic" id="ModalSubmitTitle">
                        <img src="{{asset('assets/image/logo.png')}}" alt="logo_brh" style="width: 100px;" />
                    </div>
                    <button type="button" class="close btn" style="margin: 0 0 5px 0;" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">X</span>
                    </button>
                </div>
                <div class="modal-body" style="font-size: large;">
                    <div class="col-12 mx-auto text-right">
                        หมายเลขการจอง: {{$register_no}}
                    </div>
                    <div class="col-12 mx-auto">
                        ขอบคุณที่ท่านให้ความไว้วางใจในการบริการของโรงพยาบาลกรุงเทพระยอง
                    </div>
                    <div class="col-12 mx-auto">
                        โรงพยาบาลได้รับยอดชำระค่าวัคซีนจำนวน <?= number_format($allPrice) ?> บาท จำนวน <?= $allDose ?> เข็ม
                        <div class="row col-12 mx-auto table-responsive">
                            ของผู้รับบริการ
                            <table>
                                <?php foreach ($reserve as $rowms) : ?>
                                    <tr>
                                        <td>
                                            <?= 'ชื่อ ' . $rowms->prename . ' ' . $rowms->firstname . ' ' . $rowms->lastname . ' จำนวน '
                                                . $rowms->dose . ' เข็ม เบอร์โทรศัพท์ ' . $rowms->mobile ?>
                                            <!-- ชื่อ นาย กกก นามสกุล กกก จำนวน 2 เข็ม เบอร์โทรศัพท์ 0619999999 -->
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row col-12 mx-auto">
                        <div class="col-12 mx-auto">
                            หมายเหตุ : รพ.จะติดต่อกลับ แจ้งวันรับการฉีดวัคซีน หลังจากได้รับการจัดสรรวัคซีนแล้ว
                        </div>
                        <div class="col-12 mx-auto">
                            1 . เมื่อทำการจองวัคซีนแล้ว ไม่สามารถคืนเงินได้ ดังนั้นท่านจะต้องพิจารณาให้รอบคอบก่อนตัดสินใจทำการจองวัคซีน
                        </div>
                        <div class="col-12 mx-auto">
                            2 . หากรพ.ไม่ได้รับการจัดสรรวัคซีนตามระยะเวลาที่กำหนด รพ.ยินดีคืนเงินให้เต็มจำนวน
                        </div>
                        <div class="col-12 mx-auto">
                            3. ราคานี้รวมประกันสำหรับผู้มีอาการแพ้วัคซีนแล้ว
                        </div>
                        <div class="col-2 mx-auto">&nbsp;</div>
                        <div class="col-10 mx-auto textwrap">
                            ขอให้ติดตามข่าวสารของทางโรงพยาบาลกรุงเทพระยอง ผ่านช่องทางดังนี้
                        </div>
                        <div class="col-2 mx-auto">&nbsp;</div>
                        <div class="col-10 mx-auto textwrap">
                            Website : <a target="_blank" href="https://bangkokhospitalrayong.com">https://bangkokhospitalrayong.com</a>
                        </div>
                        <div class="col-2 mx-auto">&nbsp;</div>
                        <div class="col-10 mx-auto textwrap">
                            Facebook : <a target="_blank" href="https://www.facebook.com/BangkokRayong">โรงพยาบาลกรุงเทพระยอง</a>
                        </div>
                        <div class="col-2 mx-auto">&nbsp;</div>
                        <div class="col-10 mx-auto textwrap">
                            LINE Official : <a href="https://page.line.me/bangkokrayong">@bangkokrayong</a>
                        </div>
                        <div class="col-2 mx-auto">&nbsp;</div>
                        <div class="col-10 mx-auto textwrap">
                            Instagram : <a href="https://www.instagram.com/bangkokhospitalrayong">bangkokhospitalrayong</a>
                        </div>
                    </div>
                    <br>
                    <div class="row col-12 justify-content-center">
                        <div class="text-center col">
                            <button class="col-6 col-sm-3 col-md-3 col-lg-3 col-xl-2 btn btn-info" id="btnAgree">รับทราบ</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('script')

<script>
    $(document).ready(function() {
        $(document).on('keydown', '#bdate', function() {
            return false;
        });
        Swal.fire({
            icon: 'warning',
            title: 'สำคัญ',
            text: 'กรุณาโอนเงินและแนบสลิป\nภายใน 1 ชั่วโมง',
            confirmButtonText: 'รับทราบ'
        });

        $('#btnModal').on('click', function(e) {
            $('#modalDetail').modal('toggle');
        });

        $('#btnAgree').on('click', function() {
            $('#ModalSubmit').modal('toggle');

        });

        $('#btnFormPayment').on('click', function() {
            $('#formPayment').submit();
        });

        function validslip() {
            var file = $('#fileImg').val();
            if (file == '' || file == null) {
                $('#fileImg').focus();
                return false;
            }
            var money = $('#money').val();
            if (money < 0 || money == '') {
                $('#money').focus();
                return false;
            }
            var time = String($('#time').val());
            if (time.length != 5) {
                $('#time').focus();
                return false;
            }
            var tel = String($('#tel').val());
            if (tel.length < 1) {
                $('#tel').focus();
                return false;
            }
            return true;
        }


        $('#formPayment').on('submit', function(e) {
            e.preventDefault();
            var result = validslip();
            // console.log($(this).serialize())
            if (result) {
                $.ajax({
                    url: "{{url('/saveSlip')}}",
                    method: "POST",
                    headers: {
                        'X-CSRF-TOKEN': "{{csrf_token()}}"
                    },
                    data: $(this).serialize(),
                    dataType: "JSON",
                    success: function(data) {
                        if (data.data.success == true) {
                            $('#ModalSubmit').modal('toggle');
                        }
                    }
                });
            }

        });

        $('#ModalSubmit').on('hidden.bs.modal', function() {
            Swal.fire({
                title: 'สำเร็จ',
                text: 'คุณได้ทำการชำระเงินเรียบร้อย',
                icon: 'success',
                confirmButtonText: 'ปิด',
                confirmButtonColor: '#A5DC86'
            }).then((result) => {
                if (result.isConfirmed) {
                    window.location.href = "{{url('/')}}";
                } else {
                    window.location.href = "{{url('/')}}";
                }
            });
        })


        function readURL(input) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#imgPreview').attr('src', e.target.result);
                    // console.log(e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $(document).on('change', '#fileImg', function() {
            // console.log($(this));
            var result = $(this).val();
            if (result != '') {
                var file = this.files[0];
                let mb = parseFloat(file.size / (Math.pow(10, 6)));
                if (mb > 5) {
                    Swal.fire({
                        title: 'ข้อผิดพลาด!',
                        text: 'รูปถ่ายมีขนาดเกิน 5Mb',
                        icon: 'error',
                        confirmButtonText: 'ปิด',
                        confirmButtonColor: '#F27474'

                    })
                    $('#fileImg').val('');
                } else {
                    var fileType = file["type"];
                    var validImageTypes = ["image/gif", "image/jpeg", "image/png"];
                    if ($.inArray(fileType, validImageTypes) < 0) {
                        $('#fileImg').val('');
                        $('#msg_preview').html('กรุณาเลือกรูปภาพ')
                        $('#imgPreview').attr('src', '');
                    } else {
                        readURL(this);
                        $(' #formUploadImage').submit();
                    }
                }
            } else {
                $('#imgPreview').attr('src', '');
            }
        });



        $(document).on('submit', '#formUploadImage', function(e) {
            e.preventDefault();
            $.ajax({
                url: "{{url('/uploadImage')}}",
                method: "POST",
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                contentType: false,
                processData: false,
                data: new FormData(this),
                dataType: "JSON",
                success: function(data) {
                    // console.log(data);
                    $('#hd_image').val(data.data.data.filename);
                }
            })

        });


    });
</script>
@stop