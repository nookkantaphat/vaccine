<?php $background_modal = asset('assets/image/BK_coronavirus_blue.jpg'); ?>

@extends('layout')
@section('style')
<style>
    a li.list-group-item:hover {
        background-color: #dcdcde;
    }

    input[type=text] {
        border: none;
        border-bottom: 2px solid #808080;
    }

    input[type=number] {
        border: none;
        border-bottom: 2px solid #808080;
    }

    input:required {
        border: none;
        border-bottom: 2px solid red;
    }

    input:required:valid {
        border: none;
        border-bottom: 2px solid #067423;
    }

    #fdate_dob:valid+.labelDOB {
        visibility: visible !important;
    }



    .BKForm {
        background-image: url(<?= $background_modal ?>);
        background-repeat: no-repeat;
        background-size: cover;
        border-radius: 20px;
    }

    .BKForm:before {
        opacity: 0.5;
    }

    .BorderInput {
        border: groove;
        border-color: deepskyblue;
        border-radius: 10px;
        background-color: white;
    }

    .FontTopic {
        font-size: x-large;
        text-shadow: 1px 1px 1px #0094ff;
    }

    .CheckboxSize {
        width: 1.5em;
        height: 1.5em;
    }

    .fontInvent {
        color: blue;
        font-size: 25px;
        font-family: 'Prompt', sans-serif;
        text-shadow: 2px 2px 2px #ffffff;
    }

    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    .textwrap {
        word-wrap: break-word;
        text-overflow: ellipsis;
    }
</style>
@stop

@section('body')
<div class="row col-12 mx-auto" style="margin: 70px 0 120px 0;">
    <div class="row justify-content-center" style="margin: 50px 0 5px 0;">
        <div class="col-sm-12 col-md-12  col-xl-12 " style="background-color: #dcdcde; border-radius: 5px;">
            <div class="row">
                <div class=" col-sm-4  col-md-4 col-xl-4  text-center ">
                    <span class="font-vaccine center-block">ทั้งหมด</span>&nbsp;<span id="vaccineTotal" class="font-vaccine"><?= number_format($limitdose); ?></span>
                    <img src="{{asset('assets\image\syringe_vaccine.png')}}" id="imgVaccineTotal" alt="" width="50px">
                </div>
                <div class=" col-sm-4 col-md-4 col-xl-4  text-center align-middle">
                    <span class="font-vaccine">จองแล้ว</span>&nbsp;<span id="vaccineLeft" class="font-vaccine"><?= number_format($reservedose); ?></span>
                    <img src="{{asset('assets\image\syringe_vaccine_book.png')}}" id="imgVaccineLeft" alt="" width="50px">
                </div>
                <div class=" col-sm-4 col-md-4 col-xl-4  text-center align-middle">
                    <span class="font-vaccine">เหลือ</span>&nbsp;<span id="vaccineReserved" class="font-vaccine"><?= number_format($balance); ?></span>
                    <img src="{{asset('assets\image\syringe_vaccine.png')}}" id="imgVaccineReserved" alt="" width="50px">
                </div>
            </div>
        </div>
    </div>
    <div class="alert alert-warning mt-2" role="alert" style="font-size:x-large">
        <strong>คำแนะนำ!</strong> เมื่อทำการลงทะเบียนเสร็จแล้วท่านจะต้องชำระเงินภายใน 1ชั่วโมง ไม่เช่นนั้นจะถูกตัดสิทธิ์ กรุณาเตรียมเงินของท่านสำหรับการโอนชำระออนไลน์ผ่านหมายเลขบัญชีธนาคาร
    </div>

    <div>
        <div class="col-10 mx-auto mt-2 text-left FontTopic">
            ที่อยู่ผู้ติดต่อ
        </div>
        <form id="formAddress">
            <input type="hidden" name="roundId" value="{{$roundId}}">
            <input type="hidden" name="hd_parentsId" id="hd_parentsId" value="<?= (isset($_GET['register_id'])) ? $_GET['register_id'] : '' ?>">
            <?php if (isset($_GET) && isset($_GET['register_id'])) { ?>
                <div class="row col-11 mx-auto BorderInput">
                    <div class="col-lg-3 col-sm-6 mx-auto my-2">
                        <input type="text" name="zipcode" id="zipcode" class="form-control" value="<?= $register_Info->address_zipcode ?>" placeholder="รหัสไปรษณีย์" required />
                        <div class="list-group col-lg-3 col-sm-6 mx-auto my-2" id="ziplist" style="position: absolute;">
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6 mx-auto my-2">
                        <input name="hnumber" id="hnumber" value="<?= $register_Info->address_house ?>" class="form-control" placeholder="บ้านเลขที่" required />
                    </div>
                    <div class="col-lg-2 col-sm-6 mo-auto my-2">
                        <input type="text" name="moo" id="moo" value="<?= $register_Info->address_moo ?>" class="form-control" placeholder="หมู่ที่" />
                    </div>
                    <div class="col-lg-4 col-sm-6 mx-auto my-2">
                        <input type="text" name="village" id="village" value="<?= $register_Info->address_village ?>" class="form-control" placeholder="ชื่ออาคาร หรือหมู่บ้าน" />
                    </div>
                    <div class="col-lg-4 col-sm-6 mx-auto my-2">
                        <input type="text" name="road" id="road" value="<?= $register_Info->address_street ?>" class="form-control" placeholder="ถนน" />
                    </div>
                    <div class="col-lg-4 col-sm-6 mx-auto my-2">
                        <input type="text" name="soi" id="soi" value="<?= $register_Info->address_soi ?>" class="form-control" placeholder="ซอย" />
                    </div>
                    <div class="col-lg-4 col-sm-6 mx-auto my-2">
                        <input type="text" name="tambon" id="tambon" value="<?= $register_Info->address_tambon ?>" class="form-control" placeholder="ตำบล" required />
                    </div>
                    <div class="col-lg-4 col-sm-6 mx-auto my-2">
                        <input type="text" name="district" id="district" value="<?= $register_Info->address_district ?>" class="form-control" placeholder="อำเภอ" required />
                    </div>
                    <div class="col-lg-4 col-sm-6 mx-auto my-2">
                        <input type="text" name="province" id="province" class="form-control" value="<?= $register_Info->address_province ?>" placeholder="จังหวัด" required />
                    </div>
                    <div class="col-lg-4 col-sm-6 mx-auto my-2">
                        <input type="text" name="country" id="country" value="<?= $register_Info->address_country ?>" class="form-control" placeholder="ประเทศ" required />
                    </div>
                    <div class="col-12 mx-auto my-2"></div>
                </div>
            <?php } else { ?>
                <div class="row col-11 mx-auto BorderInput">
                    <div class="col-lg-3 col-sm-6 mx-auto my-2">
                        <input type="text" value="" name="zipcode" id="zipcode" class="form-control" placeholder="รหัสไปรษณีย์" required />
                        <div class="list-group col-lg-3 col-sm-6 mx-auto my-2" id="ziplist" style="position: absolute;">
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6 mx-auto my-2">
                        <input value="" name="hnumber" id="hnumber" class="form-control" placeholder="บ้านเลขที่" required />
                    </div>
                    <div class="col-lg-2 col-sm-6 mo-auto my-2">
                        <input type="text" name="moo" id="moo" value="" class="form-control" placeholder="หมู่ที่" />
                    </div>
                    <div class="col-lg-4 col-sm-6 mx-auto my-2">
                        <input type="text" name="village" id="village" value="" class="form-control" placeholder="ชื่ออาคาร หรือหมู่บ้าน" />
                    </div>
                    <div class="col-lg-4 col-sm-6 mx-auto my-2">
                        <input type="text" name="road" id="road" value="" class="form-control" placeholder="ถนน" />
                    </div>
                    <div class="col-lg-4 col-sm-6 mx-auto my-2">
                        <input type="text" name="soi" id="soi" value="" class="form-control" placeholder="ซอย" />
                    </div>
                    <div class="col-lg-4 col-sm-6 mx-auto my-2">
                        <input type="text" name="tambon" id="tambon" class="form-control" placeholder="ตำบล" required />
                    </div>
                    <div class="col-lg-4 col-sm-6 mx-auto my-2">
                        <input type="text" name="district" id="district" class="form-control" placeholder="อำเภอ" required />
                    </div>
                    <div class="col-lg-4 col-sm-6 mx-auto my-2">
                        <input type="text" name="province" id="province" class="form-control" placeholder="จังหวัด" required />
                    </div>
                    <div class="col-lg-4 col-sm-6 mx-auto my-2">
                        <input type="text" name="country" id="country" class="form-control" placeholder="ประเทศ" required />
                    </div>
                    <div class="col-12 mx-auto my-2"></div>
                </div>
            <?php } ?>
        </form>
        <div id="person_top" class="col-10 mx-auto mt-5 text-left FontTopic">
            ข้อมูลส่วนบุคคล
        </div>
        <div id="parent">

            <?php if (isset($_GET['register_id'])) {
                $j = 1;
                foreach ($register_Reserve as $rowReserve) :
                    if ($rowReserve->idcard == '' || $rowReserve->idcard == null) {
                        $cardorpass = $rowReserve->passport;
                    } else {
                        $cardorpass = $rowReserve->idcard;
                    }
            ?>
                    <div id="row<?= $j ?>" class="row col-11 mx-auto alert alert-primary <?= $rowReserve->_id ?> " style="margin-top: 10px; margin-bottom: 10px; ">
                        <div class="row col-12">
                            <div class="row ">
                                <div class="col-12 col-sm-4 col-md-4 col-lg-4 col-xl-2">
                                    <img src="<?= $urlImg . '' . $rowReserve->cardimage ?>" class="img-fluid" />
                                </div>
                                <div class="col-12 col-sm-5 col-md-5 col-lg-5 col-xl-7 textwrap">
                                    <div class="row">
                                        <?= $j . '. ' . $rowReserve->prename . '' . $rowReserve->firstname . '' . $rowReserve->lastname
                                            . ' เลขบัตรประชาชน: ' . $cardorpass . ' วันเกิด: ' . $rowReserve->birtdate
                                        ?>
                                    </div>
                                    <div class="row">
                                        <?= 'อาชีพ: ' . $rowReserve->job . ' สถานที่ทำงาน: ' . $rowReserve->office . ' เบอร์โทรศัพท์: '
                                            . $rowReserve->mobile . ' เคยมาใช้บริการ: ' . $rowReserve->used ?>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-2 col-md-2 col-lg-2 col-xl-2  textwrap">
                                    <div class="row">
                                        จำนวน: <span class="doseUnit"><?= $rowReserve->dose ?></span> เข็ม
                                    </div>
                                </div>
                                <div class="col-12 col-sm-1 col-md-1 col-lg-1 col-xl-1 ">
                                    <button class=" btn btn-danger btn-lg cancelReserve" value="<?= $rowReserve->_id ?>">ลบ</button>
                                </div>
                            </div>
                        </div>
                    </div>
            <?php $j++;
                endforeach;
            } ?>
        </div>

        <div class="row col-11 mx-auto" id="addParents" style="margin-bottom: 10px;  <?php echo (isset($_GET['register_id'])) ? '' : 'display:none;' ?>">
            <div class="row col-6 col-sm-4 col-md-3 col-lg-2 col-xl-2">
                <button class="btn btn-outline-primary" id="addParentsBtn">เพิ่มสมาชิก</button>
            </div>
        </div>

        <div id="forminput">
            <?php if (!isset($_GET['register_id'])) { ?>

                <div class="row col-11 mx-auto BorderInput">
                    <form id="formInfo">
                        <div class="row mx-auto my-auto my-sm-2" style="font-size: large">
                            <div class="col-12 col-sm-4 col-md-3 col-lg-2 col-xl-2">
                                <span class="my-auto mr-5">ต้องการฉีด : </span>
                            </div>
                            <div class="col-6 col-sm-4 col-md-2 col-lg-2 col-xl-2">
                                <input type="radio" name="radio_dose" class="CheckboxSize radio_dose" value="1" checked />
                                <span class="my-auto mr-3"> 1 เข็ม</span>
                            </div>
                            <div class="col-6 col-sm-4  col-md-2 col-lg-2  col-xl-2">
                                <input type="radio" name="radio_dose" class="CheckboxSize radio_dose" value="2" />
                                <span class="my-auto"> 2 เข็ม</span>
                            </div>
                            <input type="hidden" class="usedDose" name="hd_dose" id="hd_dose" value="1">
                        </div>
                        <div class="row">
                            <div class="col-lg-5 col-sm-12 col-md-6 col-xl-3 mx-auto my-auto my-sm-2" style="font-size: large">
                                <div class="row">
                                    <div class="col-6 col-lg col-xl-6 mx-auto text-center">
                                        <input type="radio" name="radio_card" class="CheckboxSize rd_card" value="1" checked />
                                        <span class="my-auto mr-2">บัตรประชาชน</span>
                                    </div>
                                    <div class="col-6 col-lg  col-xl-6  mx-auto text-center">
                                        <input type="radio" name="radio_card" class="CheckboxSize rd_card" value="2" />
                                        <span class="my-auto mr-2">Passport</span>
                                    </div>
                                </div>
                                <input type="hidden" name="hd_card" id="hd_card" value="1">
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-9  mx-auto my-auto my-sm-2">
                                <input type="number" name="idcard" id="idcard" value="" class="form-control" placeholder="เลขบัตรประจำตัวประชาชน / Passport number" required />
                                <span id="errIdcardmsg" style="color: red;"></span>
                            </div>

                        </div>
                        <div class="row">
                            <div class="row col-sm-6 col-md-6  col-lg-6 col-xl-6  mx-auto my-auto text-right">
                                <span class="col-4 col-sm-2 col-md-2 col-lg-2 col-xl-2 mx-auto my-auto ">วันเกิด</span>
                                <div class="col-8 col-sm-10 col-md-10 col-lg-10 col-xl-10 mx-auto">

                                    <input type="date" name="bdate" id="bdate" required class="form-control date-input" />
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-6  col-lg-6 col-xl-6 mx-auto my-auto my-sm-2">
                                <select class="form-control" name="nametitle" id="nametitle" required style="border: none; border-bottom: 2px solid #808080;">
                                    <option value="นาย" selected="selected">นาย</option>
                                    <option value="นาง">นาง</option>
                                    <option value="น.ส.">น.ส.</option>
                                </select>
                            </div>
                        </div>
                        <div class="col col-sm-12 col-md-12 col-lg-12 col-xl-12 mx-auto">
                            <div class="row">
                                <div class="col-12 col-sm-6 col-md-6   col-lg-6 col-xl-6 mx-auto my-auto my-sm-2">
                                    <input value="" name="fname" id="fname" class="form-control" placeholder="ชื่อ" required />
                                </div>
                                <div class="col-12 col-sm-6 col-md-6    col-lg-6 col-xl-6 mx-auto my-auto my-sm-2">
                                    <input value="" name="lname" id="lname" class="form-control" placeholder="นามสกุล" required />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-lg-6  mx-auto my-auto my-sm-2 text-center">
                                <input type="number" name="tel" id="tel" value="" class="form-control" placeholder="เบอร์โทรศัพท์" required />
                            </div>
                            <div class="col-lg-6 col-sm-6 mx-auto my-auto my-sm-2 text-center">
                                <input type="text" name="job" value="" class="form-control" placeholder="อาชีพ" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-sm-6 mx-auto my-auto my-sm-2 text-center">
                                <input type="text" name="workplace" id="workplace" value="" class="form-control" placeholder="สถานที่ทำงาน" />
                            </div>
                            <div class="col-lg-6 col-sm-6 mx-auto my-auto my-sm-2 text-center">
                                เคยมาใช้บริการ <input type="checkbox" id="chkUsedto" class="CheckboxSize" />
                                <input type="hidden" id="hd_chkUsedto" ` name="hd_chkUsedto" value="0">
                            </div>
                        </div>
                        <input type="hidden" name="hd_image" id="hd_image">

                    </form>
                    <div class="row col-12 mx-auto my-3">
                        <form id="formUploadImage">
                            <div class="col-12 col-xl-3 ">
                                <span style="font-size: large">ภาพถ่ายบัตรประชาชน</span>
                                <input type="file" name="fileImg" id="fileImg" class="form-control" required />
                            </div>
                        </form>
                        <div class="col-lg-12 col-sm-5 mx-auto text-center">
                            <div class="col-12 col-lg-6 col-xl-3 mx-auto">
                                <br>
                                <img src=" {{asset('assets\image\CardID_EX.jpg')}}" id="imgPreview" class="img-fluid" />
                            </div>
                            <label style="color: red;" id="msg_preview">ตัวอย่าง ภาพถ่ายบัตรประชาชน</label>
                        </div>
                    </div>
                    <div class="col-12 mx-auto my-2 text-end ">
                        <button type="button" id="formBtn" class="btn btn-outline-primary btn-lg">จอง</button>
                    </div>
                </div>
            <?php } ?>
        </div>


        <div id="person_top" class="col-10 mx-auto mt-5 text-left FontTopic">
            ข้อมูลการจองวัคซีน
        </div>
        <div class="row col-11 mx-auto BorderInput">
            <div class="col-12 mx-auto my-2"></div>
            <div class="col-4 mx-auto my-auto my-sm-2">
                จองทั้งหมด <span id="totalDose">0</span> เข็ม
            </div>
            <div class="col-4 mx-auto my-auto my-sm-2">
                ราคา เข็มละ 1,650 บาท
            </div>
            <div class="col-4 mx-auto my-auto my-sm-2">
                รวมเป็นเงินทั้งหมด <span id="totalPrice">0</span> บาท
            </div>
            <div class="col-12 mx-auto my-2"></div>
        </div>
        <div class="col-10 mx-auto mt-5 text-left FontTopic">
            ท่านทราบข่าวสารจากช่องทางใด
        </div>
        <input type="hidden" name="hd_parentsId2" id="hd_parentsId2" value="<?= (isset($_GET['register_id'])) ? $_GET['register_id'] : '' ?>">
        <div class="row col-11 mx-auto mb-5 BorderInput" style="font-size: x-large; padding: 5px 5px 5px 5px;">
            <textarea class="form-control my-4" rows="3" name="news" id="news" placeholder="facebook line ป้ายประชาสัมพันธ์ เพื่อน หรือบุคคลในโรงพยาบาลกรุงเทพระยองแนะนำ ?"></textarea>
        </div>
        <div class="col-12 mx-auto mb-5 text-center">
            <button class="btn btn-outline-primary btn-lg" id="confirm">ชำระเงิน</button>
        </div>


    </div>
    @stop
    @section('script')
    <script>
        $(document).ready(function() {



            function Script_checkID(id) {
                if (!IsNumeric(id)) return false;
                if (id.substring(0, 1) == 0) return false;
                if (id.length != 13) return false;
                for (i = 0, sum = 0; i < 12; i++)
                    sum += parseFloat(id.charAt(i)) * (13 - i);
                if ((11 - sum % 11) % 10 != parseFloat(id.charAt(12))) return false;
                return true;
            }

            function IsNumeric(input) {
                var RE = /^-?(0|INF|(0[1-7][0-7]*)|(0x[0-9a-fA-F]+)|((0|[1-9][0-9]*|(?=[\.,]))([\.,][0-9]+)?([eE]-?\d+)?))$/;
                return (RE.test(input));
            }

            $(document).on('keyup', '#zipcode', function() {
                let zipcode = $(this).val();
                if (zipcode.length > 3) {
                    $.ajax({
                        url: "{{url('fetchDistrict')}}",
                        headers: {
                            'X-CSRF-TOKEN': "{{ csrf_token() }}"
                        },
                        method: "POST",
                        data: {
                            zipcode: $(this).val(),
                        },
                        dataType: "JSON",
                        success: function(data) {

                            if (data.data && data.data.length != 0) {
                                // console.log(data.data.data)
                                $('#ziplist').css('display', '')
                                var html = '';
                                let no = 1;
                                $.each(data.data.data, function(key, val) {
                                    html += `<a href="#" id="zipNo${no}" class="list-group-item list-group-item-action ziplistclick">
                                       ${val.zipcode_main} ${val.tambon_th}
                                       <input type="hidden" name="hd_zipcode" class="hd_zipcode" value="${val.zipcode_main}">
                                        <input type="hidden" name="hd_tambon" class="hd_tambon" value="${val.tambon_short_th}">
                                        <input type="hidden" name="hd_district" class="hd_district" value="${val.district_short_th}">
                                        <input type="hidden" name="hd_province" class="hd_province" value="${val.province_th}">
                                     </a>`;
                                    no++;
                                });
                                $('#ziplist').html(html);
                            }

                        }
                    })
                }
            });

            $(document).on('click', '.ziplistclick', function(e) {
                e.preventDefault();
                let zipid = $(this).attr('id');
                let tambonname = $("#" + zipid + " .hd_tambon").val();
                let districtname = $("#" + zipid + " .hd_district").val();
                let provincename = $("#" + zipid + " .hd_province").val();
                $('#tambon').val(tambonname);
                $('#district').val(districtname);
                $('#province').val(provincename);
                $('#country').val('ไทย');

                $('#ziplist').css('display', 'none')
            });

            $(document).on('click', '#chkUsedto', function() {
                if ($(this).prop('checked') == true) {
                    $('#hd_chkUsedto').val('1')
                } else {
                    $('#hd_chkUsedto').val('0')
                }
            });


            function readURL(input) {

                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('#imgPreview').attr('src', e.target.result);
                        // console.log(e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $(document).on('change', '#fileImg', function() {
                // console.log($(this));
                var result = $(this).val();
                if (result != '') {
                    var file = this.files[0];
                    let mb = parseFloat(file.size / (Math.pow(10, 6)));
                    if (mb > 5) {
                        Swal.fire({
                            title: 'ข้อผิดพลาด!',
                            text: 'รูปถ่ายมีขนาดเกิน 5Mb',
                            icon: 'error',
                            confirmButtonText: 'ปิด'
                        })
                        $('#msg_preview').html('กรุณาอัพรูปขนาดไม่เกิน 5 Mb');
                        $('#fileImg').val('');

                    } else {
                        var fileType = file["type"];
                        var validImageTypes = ["image/gif", "image/jpeg", "image/png"];
                        if ($.inArray(fileType, validImageTypes) < 0) {
                            $('#fileImg').val('');
                            $('#msg_preview').html('กรุณาเลือกรูปภาพ')
                            $('#imgPreview').attr('src', '');
                        } else {
                            readURL(this);
                            $(' #formUploadImage').submit();
                            $('#msg_preview').html('')
                        }
                    }


                } else {
                    $('#msg_preview').html('กรุณาเลือกรูปภาพ')
                    $('#imgPreview').attr('src', '');
                }
            });

            $(document).on('click', '.radio_dose', function() {
                let dose = $(this).val();
                $('#hd_dose').val(dose)
            });

            $(document).on('click', '.rd_card', function() {
                let card = $(this).val();
                $('#hd_card').val(card)
                if (card == '1') {
                    $('#idcard').prop('type', 'number');
                } else {
                    $('#idcard').prop('type', 'text');

                }
            });

            $(document).on('submit', '#formUploadImage', function(e) {
                e.preventDefault();
                $.ajax({
                    url: "{{url('/uploadImage')}}",
                    method: "POST",
                    headers: {
                        'X-CSRF-TOKEN': "{{ csrf_token() }}"
                    },
                    contentType: false,
                    processData: false,
                    data: new FormData(this),
                    dataType: "JSON",
                    success: function(data) {
                        // console.log(data);
                        $('#hd_image').val(data.data.data.filename);
                    }
                })

            });

            $(document).on('submit', '#formAddress', function(e) {
                e.preventDefault();
            });
            $(document).on('click', '#formBtn', function(e) {
                e.preventDefault();
                var valid = validate();
                if (valid == true) {
                    var dataString = $("#formAddress, #formInfo").serialize();
                    // console.log(dataString);
                    $.ajax({
                        url: "{{url('/addBookVaccine')}}",
                        method: "POST",
                        headers: {
                            'X-CSRF-TOKEN': "{{ csrf_token() }}"
                        },
                        data: dataString,
                        dataType: "JSON",
                        success: function(data) {
                            // console.log(data.data);
                            if (data && data.data && data.data.messageCode) {
                                if (data.data.messageCode == 'IDx1') {
                                    Swal.fire({
                                        title: 'กรุณาตรวจสอบสถานะ!',
                                        text: 'เลขบัตรประชาชนหรือพาสปอร์ตนี้มีการจองเรียบร้อย\nกรุณาตรวจสอบสถานะ',
                                        icon: 'warning',
                                        confirmButtonText: 'ปิด'
                                    });
                                } else if (data.data.messageCode == 'Dsx3') {
                                    Swal.fire({
                                        title: 'ข้อผิดพลาด!',
                                        text: 'วัคซีนสำหรับจองหมดแล้ว\nไม่สามารถจองเพิ่มได้',
                                        icon: 'error',
                                        confirmButtonText: 'ปิด'
                                    });
                                } else if (data.data.messageCode == 'Dsx2') {
                                    Swal.fire({
                                        title: 'ไม่สามารถลงทะเบียนได้!',
                                        text: 'ยอดคงเหลือต่ำกว่าจำนวนที่จอง',
                                        icon: 'warning',
                                        confirmButtonText: 'ปิด'
                                    });
                                } else if (data.data.messageCode == 'Tx1') {
                                    Swal.fire({
                                        title: 'ไม่สามารถลงทะเบียนได้!',
                                        text: 'ไม่อยู่ในช่วงเวลาการจอง',
                                        icon: 'warning',
                                        confirmButtonText: 'ปิด'
                                    });
                                }

                            } else {
                                var rowid = $('#parent div:last-child').attr('id');
                                if (rowid == '' || rowid == null) {
                                    var newid = 1;
                                } else {
                                    var newid = rowid.substr(3, 4);
                                    newid = parseInt(newid) + 1;
                                }
                                // console.log(data);
                                var img = data.apiImg + '' + data.data.reserve.cardimage;
                                if (data.data.reserve.idcard == '' || data.data.reserve.idcard == null) {
                                    var idorpass = data.data.reserve.passport;
                                } else {
                                    var idorpass = data.data.reserve.idcard;

                                }
                                if (data.ano == 1) {
                                    $('#hd_parentsId').val(data.data.register._id);
                                    $('#hd_parentsId2').val(data.data.register._id);

                                }
                                if (data.data.reserve.used == '0') {
                                    var used = 'ไม่เคย';
                                } else {
                                    var used = 'เคย';
                                }
                                let html = `<div id="row${newid}"  class="row col-11 mx-auto alert alert-primary ${data.data.reserve._id} " style="margin-top: 10px; margin-bottom: 10px; ">
                                            <div class="row col-12">
                                                <div class="row ">
                                                    <div class="col-12 col-sm-4 col-md-4 col-lg-4 col-xl-2">
                                                        <img src="${img}" class="img-fluid" />
                                                    </div>
                                                    <div class="col-12 col-sm-5 col-md-5 col-lg-5 col-xl-7 textwrap">
                                                        <div class="row">
                                                            ${newid}. ${data.data.reserve.prename} ${data.data.reserve.firstname} ${data.data.reserve.lastname}
                                                            เลขบัตรประชาชน: ${idorpass} วันเกิด: ${data.data.reserve.birtdate} 
                                                        </div>
                                                        <div class="row">
                                                            อาชีพ: ${data.data.reserve.job} สถานที่ทำงาน: ${data.data.reserve.office}
                                                            เบอร์โทรศัพท์: ${data.data.reserve.mobile} เคยมาใช้บริการ: ${used}
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-2 col-md-2 col-lg-2 col-xl-2  textwrap">
                                                    <div class="row">
                                                           จำนวน: <span class="doseUnit">${data.data.reserve.dose}</span> เข็ม
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-1 col-md-1 col-lg-1 col-xl-1 ">
                                                        <button class=" btn btn-danger btn-lg cancelReserve" value="${data.data.reserve._id}">ลบ</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>`;
                                $('#forminput').html('');
                                $('#parent').append(html);
                                $('#addParents').css('display', '');
                                totalDose();
                            }
                        }
                    });
                }
            });

            function totalDose() {
                var totalDose = 0;
                $('.doseUnit').each(function() {
                    totalDose += parseInt($(this).html());
                });
                var totalPrice = 1650 * totalDose;
                totalPrice = totalPrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                $('#totalPrice').html(totalPrice);
                $('#totalDose').html(totalDose);
            }
            <?php if (isset($_GET['register_id'])) { ?>
                totalDose();
            <?php } ?>

            function validate() {
                var zipcode = $('#zipcode').val();
                if (zipcode.length < 2) {
                    $('#zipcode').focus();
                    return false;
                }
                var hnumber = $('#hnumber').val();
                if (hnumber.length < 1) {
                    $('#hnumber').focus();
                    return false;
                }
                var tambon = $('#tambon').val();
                if (tambon.length < 1) {
                    $('#tambon').focus();
                    return false;
                }
                var district = $('#district').val();
                if (district.length < 1) {
                    $('#district').focus();
                    return false;
                }
                var province = $('#province').val();
                if (province.length < 1) {
                    $('#province').focus();
                    return false;
                }
                var idcard = $('#idcard').val();
                if (String(idcard).length < 1) {
                    $('#idcard').focus();
                    return false;
                } else {
                    if ($('#hd_card').val() == '1') {
                        if (String(idcard).length != 13) {
                            $('#idcard').focus();
                            $('#errIdcardmsg').html('กรุณากรอกให้ครบ 13 หลัก')
                            return false;
                        } else {
                            var result = Script_checkID(idcard);
                            if (result == false) {
                                $('#idcard').focus();
                                $('#errIdcardmsg').html('รหัสบัตรประชาชนไม่ถูกต้อง')
                                return false;
                            } else {
                                $('#errIdcardmsg').html('')
                            }
                        }
                    } else {
                        $('#errIdcardmsg').html('')
                    }
                }

                var strBdate = String($('#bdate').val());
                if (strBdate.length != 10) {
                    $('#bdate').focus();
                    return false;
                }

                var fname = $('#fname').val();
                if (fname.length < 1) {
                    $('#fname').focus();
                    return false;
                }
                var lname = $('#lname').val();
                if (lname.length < 1) {
                    $('#lname').focus();
                    return false;
                }
                var tel = String($('#tel').val());
                if (tel.length < 1) {
                    $('#tel').focus();
                    return false;
                }
                var fileImg = $('#fileImg').val();
                if (fileImg == null || fileImg == '') {
                    $('#fileImg').focus();
                    return false;
                }

                return true;

            }


            $(document).on('click', '#addParentsBtn', function() {
                let html = ` <div class="row col-11 mx-auto BorderInput">
                                <form id="formInfo">
                                    <div class="row mx-auto my-auto my-sm-2" style="font-size: large">
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 col-xl-2">
                                            <span class="my-auto mr-5">ต้องการฉีด : </span>
                                        </div>
                                        <div class="col-6 col-sm-4 col-md-2 col-lg-2 col-xl-2">
                                            <input type="radio" name="radio_dose" class="CheckboxSize radio_dose" value="1" checked />
                                            <span class="my-auto mr-3"> 1 เข็ม</span>
                                        </div>
                                        <div class="col-6 col-sm-4  col-md-2 col-lg-2  col-xl-2">
                                            <input type="radio" name="radio_dose" class="CheckboxSize radio_dose" value="2" />
                                            <span class="my-auto"> 2 เข็ม</span>
                                        </div>
                                        <input type="hidden" class="usedDose" name="hd_dose" id="hd_dose" value="1">
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-5 col-sm-12 col-md-6 col-xl-3 mx-auto my-auto my-sm-2" style="font-size: large">
                                            <div class="row">
                                                <div class="col-6 col-lg col-xl-6 mx-auto text-center">
                                                    <input type="radio" name="radio_card" class="CheckboxSize rd_card" value="1" checked />
                                                    <span class="my-auto mr-2">บัตรประชาชน</span>
                                                </div>
                                                <div class="col-6 col-lg  col-xl-6  mx-auto text-center">
                                                    <input type="radio" name="radio_card" class="CheckboxSize rd_card" value="2" />
                                                    <span class="my-auto mr-2">Passport</span>
                                                </div>
                                            </div>
                                            <input type="hidden" name="hd_card" id="hd_card" value="1">
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-9  mx-auto my-auto my-sm-2">
                                            <input type="number" name="idcard" id="idcard" value="" class="form-control" placeholder="เลขบัตรประจำตัวประชาชน / Passport number" required />
                                            <span id="errIdcardmsg" style="color: red;"></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="row col-sm-6 col-md-6  col-lg-6 col-xl-6  mx-auto my-auto text-right">
                                            <span class="col-4 col-sm-2 col-md-2 col-lg-2 col-xl-2 mx-auto my-auto ">วันเกิด</span>
                                            <div class="col-8 col-sm-10 col-md-10 col-lg-10 col-xl-10 mx-auto">
                                                <input type="date" name="bdate" id="bdate" required class="form-control" />
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-6  col-lg-6 col-xl-6 mx-auto my-auto my-sm-2">
                                            <select class="form-control" name="nametitle" id="nametitle" required style="border: none; border-bottom: 2px solid #808080;">
                                                <option value="นาย" selected="selected">นาย</option>
                                                <option value="นาง">นาง</option>
                                                <option value="น.ส.">น.ส.</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col col-sm-12 col-md-12 col-lg-12 col-xl-12 mx-auto">
                                        <div class="row">
                                            <div class="col-12 col-sm-6 col-md-6   col-lg-6 col-xl-6 mx-auto my-auto my-sm-2">
                                                <input value="" name="fname" id="fname" class="form-control" placeholder="ชื่อ" required />
                                            </div>
                                            <div class="col-12 col-sm-6 col-md-6    col-lg-6 col-xl-6 mx-auto my-auto my-sm-2">
                                                <input value="" name="lname" id="lname" class="form-control" placeholder="นามสกุล" required />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6 col-lg-6  mx-auto my-auto my-sm-2 text-center">
                                            <input type="number" name="tel" id="tel" value="" class="form-control" placeholder="เบอร์โทรศัพท์" required />
                                        </div>
                                        <div class="col-lg-6 col-sm-6 mx-auto my-auto my-sm-2 text-center">
                                            <input type="text" name="job" value="" class="form-control" placeholder="อาชีพ" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6 col-sm-6 mx-auto my-auto my-sm-2 text-center">
                                            <input type="text" name="workplace" value="" class="form-control" placeholder="สถานที่ทำงาน" />
                                        </div>
                                        <div class="col-lg-6 col-sm-6 mx-auto my-auto my-sm-2 text-center">
                                            เคยมาใช้บริการ <input type="checkbox" id="chkUsedto" class="CheckboxSize" />
                                            <input type="hidden" name="hd_chkUsedto" id="hd_chkUsedto" value="0">
                                        </div>
                                    </div>
                                    <input type="hidden" name="hd_image" id="hd_image">

                                </form>
                                <div class="row col-12 mx-auto my-3">
                                    <form id="formUploadImage">
                                        <div class="col-12 col-xl-3 ">
                                            <span style="font-size: large">ภาพถ่ายบัตรประชาชน</span>
                                            <input type="file" name="fileImg" id="fileImg" class="form-control" required />
                                        </div>
                                    </form>
                                    <div class="col-lg-12 col-sm-5 mx-auto text-center">
                                        <div class="col-12 col-lg-6 col-xl-3 mx-auto">
                                            <br>
                                            <img src=" {{asset('assets/image/CardID_EX.jpg')}}" id="imgPreview" class="img-fluid" />
                                        </div>
                                        <label style="color: red;" id="msg_preview">ตัวอย่าง ภาพถ่ายบัตรประชาชน</label>
                                    </div>
                                </div>
                                <div class="col-12 mx-auto my-2 text-end ">
                                    <button type="button" id="formBtn" class="btn btn-outline-primary btn-lg">จอง</button>
                                </div>
                            </div>`;
                $('#forminput').html(html);
                $('#addParents').css('display', 'none');
            });
            $(document).on('click', '.cancelReserve', function() {
                Swal.fire({
                    icon: 'warning',
                    title: 'ยกเลิกการจอง?',
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'ยืนยัน',
                    cancelButtonText: 'ยกเลิก',
                    confirmButtonColor: '#A5DC86',
                    cancelButtonColor: '#F27474',
                }).then((result) => {
                    if (result.isConfirmed) {
                        var id = $(this).val();
                        var register_id = $('#hd_parentsId').val();
                        $.ajax({
                            url: "{{url('/cancelReserve')}}",
                            headers: {
                                'X-CSRF-TOKEN': "{{ csrf_token() }}"
                            },
                            method: "POST",
                            data: {
                                id: id,
                                register_id: register_id,
                            },
                            dataType: "JSON",
                            success: function(data) {
                    
                                $('.' + id).remove();
                                totalDose();
                            }
                        });
                    }
                });

            });

            $(document).on('click', '#confirm', function() {
                var register_id = $('#hd_parentsId').val();
                var news = $('#news').val();
                $.ajax({
                    url: "{{url('/reserveConfirm')}}",
                    headers: {
                        'X-CSRF-TOKEN': "{{ csrf_token() }}"
                    },
                    method: "POST",
                    data: {
                        news: news,
                        register_id: register_id,
                    },
                    dataType: "JSON",
                    success: function(data) {
                        // console.log(data);
                        if (data.data.success == true) {
                            window.location.href = data.url + '' + data.data.register_id;
                        }
                    }
                });
            });
        });
    </script>
    @stop