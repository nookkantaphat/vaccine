@extends('layout')
<?php $background_modal = asset('assets/image/BK_coronavirus_blue.jpg'); ?>

@section('style')
<style>
    a li.list-group-item:hover {
        background-color: #dcdcde;
    }

    input[type=text] {
        border: none;
        border-bottom: 2px solid #808080;
    }

    input[type=number] {
        border: none;
        border-bottom: 2px solid #808080;
    }

    input:required {
        border: none;
        border-bottom: 2px solid red;
    }

    input:required:valid {
        border: none;
        border-bottom: 2px solid #067423;
    }

    #fdate_dob:valid+.labelDOB {
        visibility: visible !important;
    }



    .BKForm {
        background-image: url(<?= $background_modal ?>);
        background-repeat: no-repeat;
        background-size: cover;
        border-radius: 20px;
    }

    .BKForm:before {
        opacity: 0.5;
    }

    .BorderInput {
        border: groove;
        border-color: deepskyblue;
        border-radius: 10px;
        background-color: white;
    }

    .FontTopic {
        font-size: x-large;
        text-shadow: 1px 1px 1px #0094ff;
    }

    .CheckboxSize {
        width: 1.5em;
        height: 1.5em;
    }

    .fontInvent {
        color: blue;
        font-size: 25px;
        font-family: 'Prompt', sans-serif;
        text-shadow: 2px 2px 2px #ffffff;
    }

    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    .textwrap {
        word-wrap: break-word;
        text-overflow: ellipsis;
    }
</style>
@stop

@section('body')
<div class="row justify-content-center" style="margin: 110px 5px 5px 5px;">
    <div class="col-sm-12 col-md-10  col-xl-6 " style="background-color: #dcdcde; border-radius: 5px;">
        <div class="row">
            <div class=" col-sm-4  col-md-4 col-xl-4  text-center ">
                <span class="font-vaccine center-block">ทั้งหมด</span>&nbsp;<span id="vaccineTotal" class="font-vaccine"><?= number_format($limitdose); ?></span>
                <img src="{{asset('assets\image\vaccine_All.gif')}}" id="imgVaccineTotal" alt="" width="50px">
            </div>
            <div class=" col-sm-4 col-md-4 col-xl-4  text-center align-middle">
                <span class="font-vaccine">จองแล้ว</span>&nbsp;<span id="vaccineLeft" class="font-vaccine"><?= number_format($reservedose); ?></span>
                <img src="{{asset('assets\image\vaccine_booked.gif')}}" id="imgVaccineLeft" alt="" width="50px">
            </div>
            <div class=" col-sm-4 col-md-4 col-xl-4  text-center align-middle">
                <span class="font-vaccine">เหลือ</span>&nbsp;<span id="vaccineReserved" class="font-vaccine"><?= number_format($balance); ?></span>
                <img src="{{asset('assets\image\vaccine_remain.gif')}}" id="imgVaccineReserved" alt="" width="50px">
            </div>
        </div>
    </div>
</div>
<div class="row justify-content-center" style="margin: 0 0 0 0;">
    <div class="col text-center">
        <a href="" id="VaccineRegister" data-toggle="modal" data-target="#verifyModal1">
            <img src="{{asset('assets\image\ModernaVaccine.jpg')}}" alt="" class="img-fluid">
        </a>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="verifyModal1" tabindex="-1" aria-labelledby="verifyModal1Label" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body " style="  background-image: url(   <?= $background_modal ?>); background-repeat: no-repeat ;background-size: 100% 100%;">
                    <div class="row col justify-content-end">
                        <div class="d-flex justify-content-end">
                            <button type="button" class="close btn" style="margin: 0 0 5px 0;" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">X</span>
                            </button>
                        </div>
                    </div>
                    <div class="row">
                        <img src="{{asset('assets\image\ModernaAppropriate1.jpg')}}" alt="" class="img-fluid">
                    </div>
                    <div class="row justify-content-center" style="margin: 20px 0 20px 0;">
                        <div class="col col-sm col-md col-xl text-center">
                            <button class="btn btn-lg grad verify " id="verify1" style="width: 30%;background-color: #00B4BD;">
                                รับทราบ
                            </button>
                        </div>
                    </div>
                    <div class="row">
                        <img src="{{asset('assets\image\ModernaAppropriate2.jpg')}}" alt="" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="verifyModal2" tabindex="-1" aria-labelledby="verifyModal2Label" aria-hidden="true">
        <div class="modal-dialog modal-lg ">
            <div class="modal-content">
                <div class="modal-body " style="  background-image: url(   <?= $background_modal ?>); background-repeat: no-repeat ;background-size: 100% 100%;">
                    <div class="row col justify-content-end">
                        <div class="d-flex justify-content-end">
                            <button type="button" class="close btn" style="margin: 0 0 5px 0;" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">X</span>
                            </button>
                        </div>
                    </div>
                    <div class="row">
                        <img src="{{asset('assets\image\VaccineEfficacy1.jpg')}}" alt="" class="img-fluid">
                    </div>
                    <div class="row" style="margin: 20px 0 20px 0;">
                        <div class="col text-center">
                            <button class="btn grad verify" id="verify2" style="width: 30%;background-color: #00B4BD;">
                                รับทราบ
                            </button>
                        </div>
                    </div>
                    <div class="row">
                        <img src="{{asset('assets\image\ModernaAppropriate2.jpg')}}" alt="" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="verifyModal3" tabindex="-1" aria-labelledby="verifyModal3Label" aria-hidden="true">
        <div class="modal-dialog modal-lg ">
            <div class="modal-content">
                <div class="modal-body " style="  background-image: url(   <?= $background_modal ?>); background-repeat: no-repeat ;background-size: 100% 100%;">
                    <div class="row col justify-content-end">
                        <div class="d-flex justify-content-end">
                            <button type="button" class="close btn" style="margin: 0 0 5px 0;" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">X</span>
                            </button>
                        </div>
                    </div>
                    <div class="row">
                        <img src="{{asset('assets\image\VaccineEfficacy2.jpg')}}" alt="" class="img-fluid">
                    </div>
                    <div class="row" style="margin: 20px 0 20px 0;">
                        <div class="col text-center">
                            <button class="btn grad " id="verify3" style="width: 30%; background-color: #00B4BD;">
                                รับทราบ
                            </button>
                        </div>
                    </div>
                    <div class="row">
                        <img src="{{asset('assets\image\ModernaAppropriate2.jpg')}}" alt="" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal ตรวจสอบ -->
<div class="modal fade" id="checkReserveModal" tabindex="-1" aria-labelledby="checkReserveModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered ">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title fontTopic" id="ModalSubmitTitle">
                    <img src="{{asset('assets/image/logo.png')}}" alt="logo_brh" style="width: 100px;" />
                </div>
                <button type="button" class="close btn" style="margin: 0 0 5px 0;" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">X</span>
                </button>
            </div>
            <div class="modal-body " style=" background-repeat: no-repeat ;background-size: 100% 100%;">
                <div class="row justify-content-center">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        <div class="card">
                            <div class="card-body">
                                <form id="formFindReserve">
                                    <div class="row  justify-conent-center" style="margin: 10px 0 10px 0 ;">
                                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                            <div class="row">
                                                <div class="col-6 col-lg col-xl-6 mx-auto text-center">
                                                    <input type="radio" name="radio_card" class="CheckboxSize rd_card" value="1" checked />
                                                    <span class="my-auto mr-2">บัตรประชาชน</span>
                                                </div>
                                                <div class="col-6 col-lg  col-xl-6  mx-auto text-center">
                                                    <input type="radio" name="radio_card" class="CheckboxSize rd_card" value="2" />
                                                    <span class="my-auto mr-2">Passport</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row  justify-conent-center">
                                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                            <label for="idorpass">กรอกเลขบัตรประชาชน/พาสปอร์ต</label>
                                            <input type="text" class="form-control" name="idorpass" id="idorpass" placeholder="กรอกเลขบัตรประชาชน/พาสปอร์ต" required>
                                        </div>
                                    </div>
                                    <div class="row  justify-content-center" style="margin-top: 10px;margin-bottom: 10px;">
                                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                            <label for="idorpass">เบอร์โทรศัพท์</label>
                                            <input type="number" name="tel" id="tel" value="" class="form-control" placeholder="เบอร์โทรศัพท์" required />
                                        </div>
                                    </div>
                                    <div class="row  justify-content-center" style="margin-top: 5px;">
                                        <div class="col text-center">
                                            <button type="submit" class="btn btn-lg btn-primary" id="btnFindReserve" style="width: 30%;">
                                                ค้นหา
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="row justify-content-center" style="margin: 10px 0 200px 0;">
    <div class="row col-10 col-sm-10 col-md-10 col-lg-10 col-xl-12 justify-content-center  ">
        <button class="btn btn-lg grad col-12 col-sm-4 col-md-4 col-lg-3 col-xl-2" id="btnReserve" style="background-color: #B11F29; margin: 5px 5px 5px 5px;">จองวัคซีน</button>
        <button class="btn btn-lg grad col-12 col-sm-4 col-md-4 col-lg-3 col-xl-2" id="btnCheckReserve" style="background-color: #5EB8E7; margin: 5px 5px 5px 5px;">ตรวจสอบสถานะ</button>

    </div>
</div>
@stop
@section('script')
<script>
    $(document).ready(function() {


        $('#VaccineRegister').on('click', function(e) {
            e.preventDefault();
            $('#verifyModal1').modal('toggle');
        });

        $('#btnReserve').on('click', function(e) {
            e.preventDefault();
            $('#verifyModal1').modal('toggle');
        });

        $('#btnCheckReserve').on('click', function(e) {
            e.preventDefault();
            $('#checkReserveModal').modal('toggle');
        });

        $(document).on('click', '.verify', function() {
            var modalid = $(this).parents('.modal').attr('id');
            if (modalid == 'verifyModal1') {
                $('#verifyModal1').modal('toggle');
                $('#verifyModal2').modal('toggle');
            } else if (modalid == 'verifyModal2') {
                $('#verifyModal2').modal('toggle');
                $('#verifyModal3').modal('toggle');
            }
        });

        $(document).on('click', '#verify3', function() {
            $.ajax({
                url: "{{url('checkVaccine')}}",
                headers: {
                    'X-CSRF-TOKEN': "{{csrf_token()}}"
                },
                method: "POST",
                dataType: "JSON",
                success: function(data) {
                    if (data.status == true) {
                        window.location.href = "{{url('/register')}}";
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'คิวเต็ม',
                            text: 'วัคซีนได้ถูกจองหมดแล้ว',
                            confirmButtonText: `ปิด`,
                            confirmButtonColor: '#F27474'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                $('#verifyModal3').modal('toggle');
                            }
                        });
                    }
                }
            });
        });

        $(document).on('click', '.close', function() {
            var modalid = $(this).parents('.modal').attr('id');
            $('#' + modalid).modal('toggle');
        });


        $(document).on('submit', '#formFindReserve', function(e) {
            e.preventDefault();
            $.ajax({
                url: "{{url('/findReserve')}}",
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                method: "POST",
                data: $(this).serialize(),
                dataType: "JSON",
                success: function(data) {
                    if (data.data.data && data.data.data.length != 0) {
                        // console.log(data);
                        if (data.resState == 1) {
                            window.location.href = "{{url('/register')}}" + "?register_id=" + data.data.data[0].register_id;
                        } else if (data.resState == 2) {
                            window.location.href = "{{url('/payment')}}" + "?register_id=" + data.data.data[0].register_id;
                        } else if (data.resState == 3) {
                            Swal.fire({
                                icon: 'success',
                                title: 'ชำระเงินเสร็จสิ้น',
                                text: 'คุณได้ทำการชำระเงินเรียบร้อย',
                                confirmButtonText: 'ปิด',
                                confirmButtonColor: '#A5DC86'

                            });
                        }
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'ไม่พบข้อมูล',
                            text: 'กรุณากรอกข้อมูลให้ถูกต้อง',
                            confirmButtonText: 'ปิด',
                            confirmButtonColor: '#F27474'

                        });
                    }
                }
            });
        });


    });
</script>
@stop