<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>BRH Admin</title>
    <link rel="icon" href="{{asset('logo-icon.ico')}}">
    <link rel="stylesheet" href="{{asset('assets\bootstrap5\css\bootstrap.min.css')}}">
    <script src="{{asset('assets\bootstrap5\js\bootstrap.min.js')}}"></script>
    <style>
        html,
        body {
            height: 100%;
        }

        .bodybg {
            background: red;
            /* For browsers that do not support gradients */
            background: -webkit-linear-gradient(left top, lightblue, #5EB8E7, blue, violet);
            /* For Safari 5.1 to 6.0 */
            background: -o-linear-gradient(bottom right, lightblue, #5EB8E7, blue, violet);
            /* For Opera 11.1 to 12.0 */
            background: -moz-linear-gradient(bottom right, lightblue, #5EB8E7, blue, violet);
            /* For Firefox 3.6 to 15 */
            background: linear-gradient(to bottom right, lightblue, #5EB8E7, blue, violet);
            /* Standard syntax */
        }

        .btnGreen {
            background-color: #004481;
            color: white;
        }
    </style>
</head>

<body>
    <div class="h-100 row align-items-center">
        <div class="col">
            <div class="row justify-content-center">
                <div class="col-10 col-sm-9 col-md-7 col-lg-6 col-xl-5">
                    <div class="card " style="border-color: #004481;">
                        <div class="card-header">
                            <div class="row justify-content-center">
                                <div class="col-sm-9 col-md-9 col-lg-9 col-xl-8 text-center">
                                    <img src="{{asset('assets/image/logo.png')}}" alt="" class="img-fluid">
                                </div>
                            </div>

                        </div>
                        <div class="card-body">
                            <form id="formLogin" method="POST">
                                <div class="row justify-content-center">
                                    <div class="col-sm-9 col-md-9 col-lg-9 col-xl-8">
                                        <label for="username">Username </label>
                                        <input type="text" name="username" value="" class="form-control">
                                    </div>
                                </div>
                                <div class="row justify-content-center">
                                    <div class="col-sm-9 col-md-9 col-lg-9 col-xl-8">
                                        <label for="password">password</label>
                                        <input type="password" name="password" class="form-control">
                                    </div>
                                </div>
                                <div class="row justify-content-center" style="margin-top:20px;">
                                    <div class="col-sm-9 col-md-9 col-lg-9 col-xl-8">
                                        <button type="submit" class="btn btn-primary btnGreen form-control">Login</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{asset('assets\jquery\jquery-3.6.0.min.js')}}"></script>
    <script src="{{asset('assets\swal\sweetalert2.js')}}"></script>
    <script>
        $(document).ready(function() {
            $('#formLogin').on('submit', function(e) {
                e.preventDefault();
                $.ajax({
                    url: "{{url('/checkLogin')}}",
                    headers: {
                        'X-CSRF-TOKEN': "{{csrf_token()}}"
                    },
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "JSON",
                    success: function(data) {
                        if (data.status == true) {
                            window.location.href = "{{url('/admin')}}";
                        } else {
                            Swal.fire({
                                icon: 'error',
                                title: 'Login False',
                                text: 'username or password wrong',
                                confirmButtonText: 'ปิด',
                                confirmButtonColor: '#F27474'
                            });
                        }
                    }
                });

            });
        });
    </script>
</body>

</html>