<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PhpParser\JsonDecoder;

class VaccineCovid extends Controller
{
    public function index()
    {
        $host = env('HOSTAPI');
        $api_key = env('APIKEY');
        $api = '/round/list';
        $url = $host . '' . $api;
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                "api_key: $api_key",
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $res_arr = json_decode($response);
        $res_end = end($res_arr);
        $round_id = $res_end[0]->_id;
        $api = '/round/counter';
        $curl = curl_init();
        $url = $host . '' . $api;
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '{
              "round_id":"' . $round_id . '"
          }',
            CURLOPT_HTTPHEADER => array(
                'api_key: ' . $api_key,
                'Content-Type: application/json'
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $res_new = json_decode($response);
        $data['limitdose'] = $res_new->limitdose;
        $data['reservedose'] = $res_new->reservedose;
        $data['balance'] = $res_new->limitdose- $res_new->reservedose;
        echo $data['balance'];
        return view('index',$data);
    }

    public function findReserve(Request $request)
    {
        // $callbackData['formdata'] = $_POST;
        $host = env('HOSTAPI');
        $api_key = env('APIKEY');
        $api = '/register/findRegister';
        $url = $host . '' . $api;
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '{
            "cardtype":' . $_POST['radio_card'] . ',
            "q":"' . $_POST['idorpass'] . '",
            "mobile":"' . $_POST['tel'] . '"
        }',
            CURLOPT_HTTPHEADER => array(
                'api_key: ' . $api_key,
                'Content-Type: application/json'
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            $callbackData['error'] =  "cURL Error #:" . $err;
        } else {
            $dataResponse = json_decode($response);
            if ($dataResponse->data != null) {
                $register_id = $dataResponse->data[0]->register_id;
                $curlRe = curl_init();
                $apiRe = '/register/preview';
                $urlRe = $host . '' . $apiRe;
                curl_setopt_array($curlRe, array(
                    CURLOPT_URL => $urlRe,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS => '{
                  "register_id" : "' . $register_id . '"
              }',
                    CURLOPT_HTTPHEADER => array(
                        'api_key: ' . $api_key,
                        'Content-Type: application/json'
                    ),
                ));
                $responseRe = curl_exec($curlRe);
                curl_close($curlRe);
                $dataRe = json_decode($responseRe);
                $callbackData['resState'] = end($dataRe->data->status)->status_state;
                $callbackData['data'] = $dataResponse;
            } else {
                $callbackData['data'] = $dataResponse;
            }
        }
        echo json_encode($callbackData);
    }

    public function checkVaccine()
    {
        $host = env('HOSTAPI');
        $api_key = env('APIKEY');
        $api = '/round/list';
        $url = $host . '' . $api;
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                "api_key: $api_key",
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $res_arr = json_decode($response);
        $res_end = end($res_arr);
        $round_id = $res_end[0]->_id;
        $api = '/round/counter';
        $curl = curl_init();
        $url = $host . '' . $api;
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '{
              "round_id":"' . $round_id . '"
          }',
            CURLOPT_HTTPHEADER => array(
                'api_key: ' . $api_key,
                'Content-Type: application/json'
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $res_new = json_decode($response);
        if ($res_new->limitdose == $res_new->reservedose) {
            $callbackData['status'] = false;
        } else {
            $callbackData['status'] = true;
        }
        echo json_encode($callbackData);
    }
}
