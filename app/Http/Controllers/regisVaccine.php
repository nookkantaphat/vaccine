<?php

namespace App\Http\Controllers;

use CURLFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;
use Symfony\Component\HttpFoundation\Session\SessionBagProxy;

class regisVaccine extends Controller
{
    public function index(Request $request)
    {
        $host = env('HOSTAPI');
        $api_key = env('APIKEY');
        if (isset($_GET['register_id'])) {
            $curlRe = curl_init();
            $apiRe = '/register/preview';
            $urlRe = $host . '' . $apiRe;
            curl_setopt_array($curlRe, array(
                CURLOPT_URL => $urlRe,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => '{
                  "register_id" : "' . $_GET['register_id'] . '"
              }',
                CURLOPT_HTTPHEADER => array(
                    'api_key: ' . $api_key,
                    'Content-Type: application/json'
                ),
            ));
            $errRe = curl_error($curlRe);
            $responseRe = curl_exec($curlRe);
            curl_close($curlRe);
            $dataRe = json_decode($responseRe);
            if (end($dataRe->data->status)->status_state == 1) {
                $data['urlImg'] = $host . '/file/getImage/';
                $data['register_Info'] = $dataRe->data->register;
                $data['register_Reserve'] = $dataRe->data->reserve;
            } else if (end($dataRe->data->status)->status_state == 2) {
                return redirect('/payment?register_id=' . $dataRe->data->reserve[0]->register_id . '');
            } else {
                return redirect('/');
            }
        }
        $data['host'] = env('HOSTAPI');
        $curl = curl_init();
        $api = '/round/list';
        $url = $host . '' . $api;
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                "api_key: $api_key",
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $res_arr = json_decode($response);
        $res_end = end($res_arr);
        $data['roundId'] = $res_end['0']->_id;
        $api3 = '/round/counter';
        $curl3 = curl_init();
        $url3 = $host . '' . $api3;
        curl_setopt_array($curl3, array(
            CURLOPT_URL => $url3,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '{
              "round_id":"' . $res_end['0']->_id . '"
          }',
            CURLOPT_HTTPHEADER => array(
                'api_key: ' . $api_key,
                'Content-Type: application/json'
            ),
        ));
        $response3 = curl_exec($curl3);
        curl_close($curl3);
        $res_new3 = json_decode($response3);
        $data['limitdose'] = $res_new3->limitdose;
        $data['reservedose'] = $res_new3->reservedose;
        $data['balance'] = $res_new3->limitdose- $res_new3->reservedose;
        return view('register', $data);
    }

    public function zipcode()
    {
        $zipcode = $_POST['zipcode'];
        $data['host'] = env('HOSTAPI');
        $data['zipcode'] = $zipcode;
        echo json_encode($data);
    }


    public function fetchDistrict()
    {
        $callbackData = array();
        $zipcode = $_POST['zipcode'];
        $host = env('HOSTAPI');
        $api_key = env('APIKEY');
        $api = '/master/thailand/' . $zipcode;
        $url = $host . '' . $api;
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_CUSTOMREQUEST => "GET",
            // CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => array(
                // Set Here Your Requesred Headers
                'Content-Type: application/json',
                "api_key: $api_key",
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            $callbackData['status'] = false;
            $callbackData['error'] =  "cURL Error #:" . $err;
        } else {
            $callbackData['status'] = true;
            $callbackData['data'] = json_decode($response);
        }
        echo json_encode($callbackData);
    }


    public function uploadImage(Request $request)
    {
        $host = env('HOSTAPI');
        $api_key = env('APIKEY');
        $api = '/upload/card';
        $url = $host . '' . $api;
        $imgtmp = $_FILES["fileImg"]["tmp_name"];
        $imgtype = $_FILES["fileImg"]["type"];
        $imgname = $_FILES["fileImg"]["name"];
        $newfile = new CURLFile($imgtmp, $imgtype, $imgname);
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array(
                'fileUpload' =>  $newfile,
            ),
            CURLOPT_HTTPHEADER => array(
                "api_key: $api_key"
            ),
        ));
        $err = curl_error($curl);
        $response = curl_exec($curl);
        curl_close($curl);
        if ($err) {
            $callbackData['status'] = false;
            $callbackData['error'] =  "cURL Error #:" . $err;
        } else {
            $callbackData['status'] = true;
            $callbackData['data'] = json_decode($response);
        }
        echo json_encode($callbackData);
    }

    public function addBookVaccine(Request $request)
    {
        // $callbackData['formdata'] = $_POST;

        if ($_POST['hd_card'] == '1') {
            $idcard = $_POST['idcard'];
            $passport = '';
        } else {
            $idcard = '';
            $passport = $_POST['idcard'];
        }
        $host = env('HOSTAPI');
        $api_key = env('APIKEY');
        $api = '/register';
        $url = $host . '' . $api;
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '{
                "round_id":"' . $_POST['roundId'] . '",
                "address_house": "' . $_POST['hnumber'] . '",
                "address_moo": "' . $_POST['moo'] . '",
                "address_village":"' . $_POST['village'] . '",
                "address_street": "' . $_POST['road'] . '",
                "address_soi": "' . $_POST['soi'] . '",
                "address_tambon": "' . $_POST['tambon'] . '",
                "address_district": "' . $_POST['district'] . '",
                "address_province": "' . $_POST['province'] . '",
                "address_zipcode": "' . $_POST['zipcode'] . '",
                "address_country": "' . $_POST['country'] . '",
                "news":"",
                "register_id":"' . $_POST['hd_parentsId'] . '",
                "reserve": {
                    "prename": "' . $_POST['nametitle'] . '",
                    "firstname": "' . $_POST['fname'] . '",
                    "lastname": "' . $_POST['lname'] . '",
                    "cardtype": ' . $_POST['hd_card'] . ', 
                    "idcard": "' . $idcard . '",
                    "passport": "' . $passport . '",
                    "birtdate": "' . $_POST['bdate'] . '",
                    "job": "' . $_POST['job'] . '",
                    "office": "' . $_POST['workplace'] . '",
                    "mobile": "' . $_POST['tel'] . '",
                    "dose": "' . $_POST['hd_dose'] . '",
                    "price": "1650",
                    "cardimage": "' . $_POST['hd_image'] . '",
                    "used":' . $_POST['hd_chkUsedto'] . '
                }
            }',
            CURLOPT_HTTPHEADER => array(
                "api_key: $api_key",
                'Content-Type: application/json'
            ),
        ));
        $err = curl_error($curl);
        $response = curl_exec($curl);
        curl_close($curl);
        if ($_POST['hd_parentsId'] == '') {
            $callbackData['ano'] = '1';
        } else {
            $callbackData['ano'] = '2';
        }
        if ($err) {
            $callbackData['status'] = false;
            $callbackData['error'] =  "cURL Error #:" . $err;
        } else {
            $callbackData['status'] = true;
            $callbackData['data'] = json_decode($response);
            $callbackData['apiImg'] = env('HOSTAPI') . '/file/getImage/';
        }
        echo json_encode($callbackData);
    }

    public function cancelReserve()
    {
        $callbackData['formdata'] = $_POST;
        $id = $_POST['id'];
        $register_id = $_POST['register_id'];
        $host = env('HOSTAPI');
        $api_key = env('APIKEY');
        $api = '/register/deleteReserve';
        $url = $host . '' . $api;
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '{
                "register_id":"' . $register_id . '",
                "reserve_id":"' . $id . '"
            }',
            CURLOPT_HTTPHEADER => array(
                'api_key: ' . $api_key,
                'Content-Type: application/json'
            ),
        ));
        $err = curl_error($curl);
        $response = curl_exec($curl);
        curl_close($curl);
        if ($err) {
            $callbackData['status'] = false;
            $callbackData['error'] =  "cURL Error #:" . $err;
        } else {
            $callbackData['status'] = true;
            $callbackData['data'] = json_decode($response);
        }
        echo json_encode($callbackData);
    }

    public function reserveConfirm()
    {
        $register_id = $_POST['register_id'];
        $news = $_POST['news'];
        $callbackData['formdata'] = $_POST;
        $host = env('HOSTAPI');
        $api_key = env('APIKEY');
        $api = '/register/confirm';
        $url = $host . '' . $api;
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '{
                "register_id" : "' . $register_id . '",
                "news" :  "' . $news . '"
            }',
            CURLOPT_HTTPHEADER => array(
                'api_key: ' . $api_key,
                'Content-Type: application/json'
            ),
        ));

        $err = curl_error($curl);
        $response = curl_exec($curl);
        curl_close($curl);

        if ($err) {
            $callbackData['status'] = false;
            $callbackData['error'] =  "cURL Error #:" . $err;
        } else {
            $callbackData['url'] = route('/payment') . '?register_id=';
            $callbackData['status'] = true;
            $callbackData['data'] = json_decode($response);
        }
        echo json_encode($callbackData);
    }
}
