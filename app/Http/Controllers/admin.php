<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class admin extends Controller
{
    public function index(Request $request)
    {

        return view('login');
    }

    public function checkLogin(Request $request)
    {
        if ($_POST['username'] == 'admin' && $_POST['password'] == 'admin1234') {
            $callbackData['status'] = true;
            $request->session()->put(['login' => true]);
        } else {
            $callbackData['status'] = false;
        }
        echo json_encode($callbackData);
    }

    public function admin(Request $request)
    {
        date_default_timezone_set('Asia/Bangkok');
        $host = env('HOSTAPI');
        $api_key = env('APIKEY');
        $api = '/round/list';
        $url = $host . '' . $api;
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                "api_key: $api_key",
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $res_arr = json_decode($response);
        $res_end = end($res_arr);
        $round_id = $res_end[0]->_id;
        $api = '/admin/getRegister';
        $url = $host . '' . $api;
        $curl2 = curl_init();
        curl_setopt_array($curl2, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '{
            "round_id":"' . $round_id . '"
        }',
            CURLOPT_HTTPHEADER => array(
                'api_key: ' . $api_key,
                'Content-Type: application/json'
            ),
        ));
        $response2 = curl_exec($curl2);
        curl_close($curl2);
        $data['register'] = json_decode($response2)->data;
        $data['round'] = $round_id;
        $data['urlImg'] = $host . '/file/getImage/';
        // echo '<pre>';
        // print_r($round_id);
        // echo '</pre>';
        // echo '<pre>';
        // print_r($data['register']);
        // echo '</pre>';
        return view('admin', $data);
    }

    public function test(Request $request)
    {
        $request->session()->flush();
    }

    public function reject()
    {
        $host = env('HOSTAPI');
        $api_key = env('APIKEY');
        $api = '/admin/addStatus';
        $url = $host . '' . $api;
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '{
                "register_id":"' . $_POST['hd_registerId'] . '",
                "status":"reject",
                "remark":"' . $_POST['remark'] . '"
            }',
            CURLOPT_HTTPHEADER => array(
                'api_key: ' . $api_key,
                'Content-Type: application/json'
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        $callbackData['data'] = json_decode($response);

        echo json_encode($callbackData);
    }

    public function confirm()
    {
        $host = env('HOSTAPI');
        $api_key = env('APIKEY');
        $api = '/admin/addStatus';
        $url = $host . '' . $api;
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '{
                "register_id":"' . $_POST['register_id'] . '",
                "status":"confirm",
                "remark":""
            }',
            CURLOPT_HTTPHEADER => array(
                'api_key: ' . $api_key,
                'Content-Type: application/json'
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        $callbackData['data'] = json_decode($response);
        // $callbackData['data'] = $_POST;
        echo json_encode($callbackData);
    }

    public function logout(Request $request)
    {
        $request->session()->flush();
        return redirect()->route('/login');
    }
}
