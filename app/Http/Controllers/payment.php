<?php

namespace App\Http\Controllers;

use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Date;
use PDO;
use Symfony\Component\VarDumper\Cloner\Data;

class payment extends Controller
{
    public function index()
    {
        if (isset($_GET['register_id'])) {
            $register_id = $_GET['register_id'];
            $host = env('HOSTAPI');
            $api_key = env('APIKEY');
            $api = '/register/preview';
            $url = $host . '' . $api;
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => '{
                    "register_id" : "' . $register_id . '"
                }',
                CURLOPT_HTTPHEADER => array(
                    'api_key: ' . $api_key,
                    'Content-Type: application/json'
                ),
            ));
            $err = curl_error($curl);
            $response = curl_exec($curl);
            curl_close($curl);
            $res_arr = json_decode($response);
            if (end($res_arr->data->status)->status_state == 1) {
                return redirect('/register?register_id=' . $res_arr->data->reserve[0]->register_id . '');
            } else if (end($res_arr->data->status)->status_state == 2) {

                $data['register_no'] = $res_arr->data->register->register_no;
                date_default_timezone_set('Asia/Bangkok');
                $timeAdd1Hour = strtotime(end($res_arr->data->reserve)->created_at) + (60 * 60);
                $timeLast =  date('H:i:s', $timeAdd1Hour);
                $data['timeLast'] = $timeLast;
                $data['register_id'] = $res_arr->register_id;
                $data['reserve'] = $res_arr->data->reserve;
                return view('payment', $data);
            } else {
                return redirect()->route('/');
            }
        } else {
            return redirect()->route('/');
        }
    }

    public function saveSlip()
    {
        $callbackData['data'] = $_POST;
        $host = env('HOSTAPI');
        $api_key = env('APIKEY');
        $api = '/register/saveSlip';
        $url = $host . '' . $api;
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '{
                "register_id":"' . $_POST['hd_parentsId'] . '",
                "filename":"' . $_POST['hd_image'] . '",
                "money":' . $_POST['money'] . ',
                "transfer_time":"' . $_POST['time'] . '",
                "mobile":"' . $_POST['tel'] . '"
            }',
            CURLOPT_HTTPHEADER => array(
                'api_key: ' . $api_key,
                'Content-Type: application/json'
            ),
        ));
        $err = curl_error($curl);
        $response = curl_exec($curl);
        curl_close($curl);

        if ($err) {
            $callbackData['status'] = false;
            $callbackData['error'] =  "cURL Error #:" . $err;
        } else {
            $callbackData['url'] = route('/payment') . '?register_id=';
            $callbackData['status'] = true;
            $callbackData['data'] = json_decode($response);
        }

        echo json_encode($callbackData);
    }
}
