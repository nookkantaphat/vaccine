<?php

use App\Http\Controllers\admin;
use App\Http\Controllers\login;
use App\Http\Controllers\payment;
use App\Http\Controllers\regisVaccine;
use App\Http\Controllers\VaccineCovid;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


// GET
Route::get('/', [VaccineCovid::class, 'index'])->name('/');
Route::get('/register', [regisVaccine::class, 'index'])->name('/register');
Route::get('/payment', [payment::class, 'index'])->name('/payment');
Route::get('/login', [admin::class, 'index'])->name('/login')->middleware('mdLogin');
Route::get('/admin', [admin::class, 'admin'])->name('/admin')->middleware('mdCheckLogin');
Route::get('/logout', [admin::class, 'logout'])->name('/logout');

// POST
Route::post('/checkVaccine', [VaccineCovid::class, 'checkVaccine']);
Route::post('/findReserve', [VaccineCovid::class, 'findReserve']);
Route::post('/fetchDistrict', [regisVaccine::class, 'fetchDistrict']);
Route::post('/zipcode', [regisVaccine::class, 'zipcode']);
Route::post('/uploadImage', [regisVaccine::class, 'uploadImage']);
Route::post('/addBookVaccine', [regisVaccine::class, 'addBookVaccine']);
Route::post('/cancelReserve', [regisVaccine::class, 'cancelReserve']);
Route::post('/reserveConfirm', [regisVaccine::class, 'reserveConfirm']);
Route::post('/saveSlip', [payment::class, 'saveSlip']);
Route::post('/checkLogin', [admin::class, 'checkLogin']);
Route::post('/reject', [admin::class, 'reject']);
Route::post('/confirm', [admin::class, 'confirm']);

